import requests
key = open('/home/jonathan/.config/jira/token')
apikey = key.readline().strip()
r = requests.get(
        'https://omnispear.atlassian.net/rest/api/2/search?jql=assignee=gilsonj',
        auth=('jgilson@omnispear.com', apikey))

issues = [v for v in r.json()['issues']]
for issue in issues:
    print("# " + issue['fields']['project']['name'])
    print('\n')
    print(issue['fields']['description'])
    print('\n')
    print(issue['fields']['priority']['name'])
    print(issue['fields']['created'])
    print('\n')
    print(issue['fields']['creator']['displayName'])
    print(issue['fields']['creator']['emailAddress'])
    print('\n')
    print(issue['fields']['timeestimate'])
    print('\n')
    print(issue['fields']['timespent'])
    # print('\n')
    # print(issue['fields']['subtasks'])
